# Otwarty słownik frekwencyjny leksemów V.06.2009
(c) Jerzy Kazojć 2009

Licencja GPL
Słownik zawiera 40384 leksemy.

----

Nie jestem autorem tego słownika. Dodaję do niego jedynie wersję tekstową wygenerowaną za pomocą `pdftotext` i odrobiny
pracy ręcznej.